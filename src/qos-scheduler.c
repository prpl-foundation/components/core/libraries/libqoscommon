/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>


#include "qos-scheduler.h"
#include "qos-assert.h"
#include "qos-type.h"

#define ME "scheduler"

amxd_object_t* qos_scheduler_get_dm_object(const qos_scheduler_t* const scheduler) {
    amxd_object_t* object = NULL;

    when_null(scheduler, exit);
    object = scheduler->dm_object;

exit:
    return object;
}

qos_status_t qos_scheduler_dm_get_status(const qos_scheduler_t* const scheduler) {
    qos_status_t status = QOS_STATUS_DISABLED;
    char* str_status = NULL;

    when_null(scheduler, exit);

    str_status = amxd_object_get_value(cstring_t, scheduler->dm_object, "Status", NULL);
    when_str_empty(str_status, exit);

    status = qos_status_from_string(str_status);
    free(str_status);

exit:
    return status;
}

qos_scheduler_algorithm_t qos_scheduler_dm_get_scheduler_algorithm(const qos_scheduler_t* const scheduler) {
    qos_scheduler_algorithm_t algo = QOS_SCHEDULER_ALGORITHM_SP;
    char* str_algo = NULL;

    when_null(scheduler, exit);

    str_algo = amxd_object_get_value(cstring_t, scheduler->dm_object, "SchedulerAlgorithm", NULL);
    when_str_empty(str_algo, exit);

    algo = qos_scheduler_algorithm_from_string(str_algo);
    free(str_algo);

exit:
    return algo;
}

int qos_scheduler_dm_set_status(const qos_scheduler_t* const scheduler, qos_status_t status) {
    int retval = -1;
    const char* str_status = NULL;
    amxd_object_t* object = NULL;
    const char* param = "Status";
    amxd_status_t amx_status = amxd_status_ok;

    if(QOS_STATUS_LAST <= status) {
        goto exit;
    }

    when_null(scheduler, exit);
    object = qos_scheduler_get_dm_object(scheduler);
    when_null(object, exit);

    str_status = qos_status_to_string(status);
    amx_status = amxd_object_set_value(cstring_t, object, param, str_status);
    when_failed(amx_status, exit);

    retval = 0;

exit:
    return retval;
}

int qos_scheduler_dm_set_enable(const qos_scheduler_t* const scheduler,
                                const bool enable) {
    int retval = -1;
    amxd_object_t* object = NULL;
    const char* param = "Enable";
    amxd_status_t status = amxd_status_ok;

    when_null(scheduler, exit);
    object = qos_scheduler_get_dm_object(scheduler);
    when_null(object, exit);

    status = amxd_object_set_value(bool, object, param, enable);
    when_failed(status, exit);

    retval = 0;

exit:
    return retval;
}

int32_t qos_scheduler_dm_set_shaping_rate(const qos_scheduler_t* const scheduler,
                                          int32_t shaping_rate) {
    int retval = -1;
    amxd_status_t status = amxd_status_ok;

    when_null(scheduler, exit);

    if(shaping_rate < -1) {
        shaping_rate = -1;
    }

    status = amxd_object_set_value(int32_t, scheduler->dm_object, "ShapingRate", shaping_rate);
    when_failed(status, exit);

    retval = 0;

exit:
    return retval;
}

int32_t qos_scheduler_dm_set_assured_rate(const qos_scheduler_t* const scheduler,
                                          int32_t assured_rate) {
    int retval = -1;
    amxd_status_t status = amxd_status_ok;

    when_null(scheduler, exit);

    if(assured_rate < -1) {
        assured_rate = -1;
    }

    status = amxd_object_set_value(int32_t, scheduler->dm_object, "AssuredRate", assured_rate);
    when_failed(status, exit);

    retval = 0;

exit:
    return retval;
}

int qos_scheduler_init(qos_scheduler_t* const scheduler,
                       amxd_object_t* const scheduler_instance) {
    int retval = -1;

    when_null(scheduler, exit);
    when_null(scheduler_instance, exit);
    when_not_null(scheduler_instance->priv, exit);

    memset(scheduler, 0, sizeof(qos_scheduler_t));

    scheduler->dm_object = scheduler_instance;
    scheduler_instance->priv = (void*) scheduler;

    retval = 0;

exit:
    return retval;
}

int qos_scheduler_new(qos_scheduler_t** scheduler,
                      amxd_object_t* scheduler_instance) {
    int retval = -1;

    when_null(scheduler, exit);
    when_null(scheduler_instance, exit);
    when_not_null(*scheduler, exit);
    when_not_null(scheduler_instance->priv, exit);

    *scheduler = (qos_scheduler_t*) calloc(1, sizeof(qos_scheduler_t));
    when_null(*scheduler, exit);

    retval = qos_scheduler_init(*scheduler, scheduler_instance);
    when_failed(retval, failed);

exit:
    return retval;

failed:
    qos_scheduler_delete(scheduler);
    return retval;
}

int qos_scheduler_deinit(qos_scheduler_t* const scheduler) {
    int retval = -1;

    when_null(scheduler, exit);

    if(scheduler->dm_object) {
        scheduler->dm_object->priv = NULL;
        scheduler->dm_object = NULL;
    }

    free(scheduler->intf_name);
    scheduler->intf_name = NULL;

    retval = 0;

exit:
    return retval;
}

int qos_scheduler_delete(qos_scheduler_t** scheduler) {
    int retval = -1;
    qos_scheduler_t* scheduler_ptr = NULL;

    when_null(scheduler, exit);
    when_null(*scheduler, exit);

    scheduler_ptr = *scheduler;

    retval = qos_scheduler_deinit(scheduler_ptr);
    when_failed(retval, exit);

    free(scheduler_ptr);
    *scheduler = NULL;
    retval = 0;

exit:
    return retval;
}
