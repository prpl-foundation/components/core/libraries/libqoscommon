/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__QOS_TYPE_H__)
#define __QOS_TYPE_H__

#ifdef __cplusplus
extern "C"
{
#endif

/**
   @file
   @brief
   QoS Common type header file
 */

/**
 * @mainpage
 * lib_qoscommon is library containing functions that facilitates the use of the data-model representation provided by the tr181-qos plugins.
 * This library provides to the user new types to help him manipulate explicit data rather than abstract amxd objects:
 *  - qos_shaper_t @ref Shaper
 *  - qos_scheduler_t @ref Scheduler
 *  - qos_queue_t @ref Queue
 *  - qos_queue_stats_t @ref Queue_Stats
 *  - various enums and utility functions @ref Utility
 */

/**
 * @defgroup Utility 5. The utility functions and enums types
 * @brief
 * Enumerations in the data-model are defined as strings of characters.
 * This library provides enumerations as form of C enum as well as utility functions to convert enum type to string and string to enum type.
 */


/**
 * @ingroup Utility
 * @brief
 * Defined by tr181 standard, QoS objects have a parameter named status.
 * This enumeration reflects the different states an object can be in.
 *  - QOS_STATUS_DISABLED
 *  - QOS_STATUS_ENABLED
 *  - QOS_STATUS_ERROR_MISCONFIGURED
 *  - QOS_STATUS_ERROR
 *  - QOS_STATUS_LAST
 */
typedef enum _qos_status {
    QOS_STATUS_DISABLED,
    QOS_STATUS_ENABLED,
    QOS_STATUS_ERROR_MISCONFIGURED,
    QOS_STATUS_ERROR,
    QOS_STATUS_LAST
} qos_status_t;

/**
 * @ingroup Utility
 * @brief
 * This enumeration details scheduling algorithms that can be use by the scheduler.
 *  - QOS_SCHEDULER_ALGORITHM_WFQ (Weighted Fair Queueing)
 *  - QOS_SCHEDULER_ALGORITHM_WRR (Weighted Round Robin)
 *  - QOS_SCHEDULER_ALGORITHM_SP (Strict priority)
 *  - QOS_SCHEDULER_ALGORITHM_HTB (Not tr181 approved)
 *  - QOS_SCHEDULER_ALGORITHM_DRR (Not tr181 approved)
 *  - QOS_SCHEDULER_ALGORITHM_TBF
 *  - QOS_SCHEDULER_ALGORITHM_FQ_CODEL
 *  - QOS_SCHEDULER_ALGORITHM_LAST
 */
typedef enum __qos_scheduler_algorithm {
    QOS_SCHEDULER_ALGORITHM_WFQ,
    QOS_SCHEDULER_ALGORITHM_WRR,
    QOS_SCHEDULER_ALGORITHM_SP,
    QOS_SCHEDULER_ALGORITHM_HTB,
    QOS_SCHEDULER_ALGORITHM_SP_WRR,
    QOS_SCHEDULER_ALGORITHM_DRR,
    QOS_SCHEDULER_ALGORITHM_TBF,
    QOS_SCHEDULER_ALGORITHM_FQ_CODEL,
    QOS_SCHEDULER_ALGORITHM_LAST
} qos_scheduler_algorithm_t;

/**
 * @ingroup Utility
 * @brief
 * This enumeration details drop algorithms that can be use when the queue is congested.
 *  - QOS_QUEUE_DROP_ALGORITHM_RED (Random Early Detection)
 *  - QOS_QUEUE_DROP_ALGORITHM_DT (Drop Tail)
 *  - QOS_QUEUE_DROP_ALGORITHM_WRED (Weighted RED)
 *  - QOS_QUEUE_DROP_ALGORITHM_BLUE (active queue management)
 *  - QOS_QUEUE_DROP_ALGORITHM_LAST
 */
typedef enum _qos_queue_drop_algorithm {
    QOS_QUEUE_DROP_ALGORITHM_RED,
    QOS_QUEUE_DROP_ALGORITHM_DT,
    QOS_QUEUE_DROP_ALGORITHM_WRED,
    QOS_QUEUE_DROP_ALGORITHM_BLUE,
    QOS_QUEUE_DROP_ALGORITHM_SFQ,
    QOS_QUEUE_DROP_ALGORITHM_LAST
} qos_queue_drop_algorithm_t;

const char* qos_status_to_string(const qos_status_t status);
qos_status_t qos_status_from_string(const char* status);
const char* qos_scheduler_algorithm_to_string(const qos_scheduler_algorithm_t algorithm);
qos_scheduler_algorithm_t qos_scheduler_algorithm_from_string(const char* algorithm);
const char* qos_queue_drop_algorithm_to_string(const qos_queue_drop_algorithm_t algorithm);
qos_queue_drop_algorithm_t qos_queue_drop_algorithm_from_string(const char* const algorithm);

#ifdef __cplusplus
}
#endif

#endif // __QOS_TYPE_H__
